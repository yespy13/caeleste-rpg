<?php

Route::get('/statsbycharacter/{id}', function ($id) {
    $results = DB::select(
        'select sbc.id_stat, sbc.level, s.name
        from stats_by_character sbc, stats s 
        where sbc.id_character=:id and sbc.id_stat = s.id
        order by id_stat', 
        [
            'id' => $id,
        ]
    );
    return response()->json($results, 200);
});

Route::post('/statsbycharacter/{id}', function () {
    $data = request()->all();

    DB::insert(
        "
        insert into stats_by_character (id_character, id_stat, level)
        values (:id_character, :id_stat, :level)
    ",
        $data
    );
});