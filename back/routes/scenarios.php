<?php

Route::get('/scenarios/{phase}', function ($phase) {   
    $results = DB::select('select id, text, phase from scenarios where phase=:phase order by id',
        [
            'phase' => $phase
        ]
    );
    return response()->json($results, 200);
});