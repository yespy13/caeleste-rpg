<?php

Route::post('/characters/', function () {
    $data = request()->all();
    var_dump($data);
    DB::insert(
        "insert into characters (name, class_id, user_id, character_created)
        values (:name, :class_id, :user_id, :character_created)",        
        [   
            'name' => $data['name'],
            'class_id' => $data['class_id'],
            'user_id' => $data['user_id'],
            'character_created' => $data['character_created']
        ]
    );
});

Route::get('/characters/', function (Request $request) {   
    $results = DB::select('select id, name, class_id, user_id,
    strength, perception, endurance, charisma, intelligence, agility,
    level, health, experience, karma, reputation, character_created
    from characters order by id');
    return response()->json($results, 200);
});

Route::delete('/characters/{id}', function ($id) {
    $data = request()->all();

    DB::delete('delete from characters where id=:id',
        [
            'id' => $id,
        ]
    );
    $result = [
        'succesful' => 'Character deleted',
    ];
    return response() -> json($result, 200);
});

Route::patch('/characters/{id}', function ($id) {
    $data = request()->all();

    $update_statements = array_map(function ($key) {
        return "$key = :$key";
    }, array_keys($data));

    $data['id'] = $id;

    DB::update(
        'update characters SET ' . join(', ', $update_statements) . ' where id = :id',
        $data
    );
});