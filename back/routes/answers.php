<?php

Route::get('/answers/{scenario_id}', function ($scenario_id) {   
    $results = DB::select('select id, text, stat_to_increase, increase_points from answers where scenario_id=:scenario_id order by id',
        [
            'scenario_id' => $scenario_id
        ]
    );
    return response()->json($results, 200);
});