import { shallowMount } from '@vue/test-utils'
import StartName from '@/components/start/StartName.vue'

test("emite evento click", async () => {
    const wrapper = shallowMount(StartName);

    expect(wrapper.emitted().setName).toBe(undefined)

    const input = wrapper.find('#textboxName')
    const submit = wrapper.find('button')
    input.setValue("alice")
    submit.trigger('click')
    await wrapper.vm.$nextTick()

  // Comprobar la emisión.
    expect(wrapper.emitted().pcName.length).toBe(1)
    expect(wrapper.emitted().pcName[0]).toEqual(["alice"]) 
});
