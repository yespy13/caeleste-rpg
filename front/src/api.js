const baseUrl = "/api/";
const baseUrlStats = baseUrl + "stats/";
const baseUrlCharacters = baseUrl + "characters/";
const baseUrlScenarios = baseUrl + "scenarios/";
const baseUrlAnswers = baseUrl + "answers/";

export default {
	// Stats
	async generateStatsData() {
		const result = await fetch(`${baseUrlStats}`);
		return await result.json();
	},
	// Character
	async createCharacter(character) {
		await fetch(
			`${baseUrlCharacters}`,
			{
				method: 'POST',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(character)
			}
		);
	},
	async getCharacter() {
		const result = await fetch(`${baseUrlCharacters}`);
		let characters = await result.json();
		return characters[0];
	},
	async deleteCharacter(id) {
		await fetch(`${baseUrlCharacters}` + id, 
			{
				method: "DELETE",
				headers: {
					"Content-Type": "application/json"
				}
			}
		)		
	},
	async updateCharacter(character) {
		character.health = 80 + (parseInt(character.endurance) * 5) + (parseInt(character.level) - 1) * (parseInt(character.endurance)/2 + 2.5);
		await fetch( `${baseUrlCharacters}` + character.id,
			{
				method: 'PATCH',
				headers: {
					'Content-type': 'application/json'
				},
				body: JSON.stringify(character)
			}
		);
	},
	// Scenario
	async getScenario(phase) {
		console.log("Fase:", phase)
		const result = await fetch(`${baseUrlScenarios}` + phase);
		return await result.json();
	},
	// Answer
	async getAnswers(scenario) {
		const result = await fetch(`${baseUrlAnswers}` + scenario);
		return await result.json();
	}
}
