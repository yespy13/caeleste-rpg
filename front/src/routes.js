import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router)

const router = new Router({
  routes: [{
    path: '/storystats',
    component: function (resolve) {
      require(['@/components/StoryStats/StoryStatsPage.vue'], resolve)
    },
  },
  {
    path: '/',
    component: function (resolve) {
      require(['@/components/Start/StartPage.vue'], resolve);
    }
  },
  {
    path: '/manualstats',
    component: function (resolve) {
      require(['@/components/ManualStats/ManualStatsPage.vue'], resolve);
    }
  },
  {
    path: '/profile',
    component: function (resolve) {
      require(['@/components/Profile/ProfilePage.vue'], resolve);
    }
  },
  {
    path: '/about',
    component: function (resolve) {
      require(['@/components/About/AboutPage.vue'], resolve);
    }
  }
  ],
});

export default router